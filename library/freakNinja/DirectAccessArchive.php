<?php
namespace freakNinja;
/**
 * DirectAccessArchive
 * 
 * @author "valerio tesei" <v@freak.ninja>
 *
 */
class DirectAccessArchive
{
	private $_version    = '0.1';
	private $_dataFolder = null;
	private $_address    = null;
	private $_options    = null;
	
	public $logLevel = 0;
	
	public function __construct($dataFolder)
	{
		if(!is_readable($dataFolder) ) {
			$this->_exception("data folder is not readable");
		}
		
		if(!is_writable($dataFolder) ) {
			$this->_exception("data folder is not writable");
		}
		
		if(!function_exists('mb_strlen') || !function_exists('mb_strlen') ) {
			$this->_exception("mb_strlen() function is required");
		} 
		
		$this->_dataFolder = realpath($dataFolder);
	}
	
	/**
	 * Returns an address folder path
	 * 
	 * @param mixed $address
	 * @return string
	 */
	public function dirname($address)
	{
		$address = (isset($address['address'])) ? md5($address['address']) : md5($address);
		$addressDirectory = substr($address, 0, 5);
		$addressDirectory = "{$this->_dataFolder}/{$addressDirectory}";
		return $addressDirectory;
	}
	
	/**
	 * Returns an address basename/struct file path
	 * 
	 * @param mixed $address
	 * @return string
	 */
	public function basename($address)
	{
		$address =  (isset($address['address'])) ? $address['address'] : $address;
		$addressDirectory = $this->dirname($address);
		$address = md5($address);
		return "{$addressDirectory}/{$address}";
	}
	
	/**
	 * Lookup an address
	 * 
	 * @param string $address
	 * @return array
	 */
	public function lookup($address)
	{
		$address =  (isset($address['address'])) ? $address['address'] : $address;
		if($address == '') {
			$this->_exception("invalid address");
		}
		$addressDirectory = $this->dirname($address);
		$structFile       = $this->basename($address);
		
		$daaBase = array(
			'address' => $address,
			'index'   => "{$structFile}.idx",
			'digest'  => "{$structFile}.digest"
		);
		
		$daaBase['exists'] = (file_exists($daaBase['index']) && file_exists($daaBase['digest']) ) ;
		$daaBase['readonly'] = !(is_writable($daaBase['index']) && is_writable($daaBase['digest'])) ;
		
		$this->_log(9, __FUNCTION__, __LINE__ , 'dirname'  , $addressDirectory );
		$this->_log(9, __FUNCTION__, __LINE__ , 'struct'   , $structFile );
		$this->_log(9, __FUNCTION__, __LINE__ , 'index'    , $daaBase['index'] );
		$this->_log(9, __FUNCTION__, __LINE__ , 'digest'   , $daaBase['digest']  );
		$this->_log(9, __FUNCTION__, __LINE__ , 'exists'   , $daaBase['exists'] ? 'yes':'no');
		$this->_log(9, __FUNCTION__, __LINE__ , 'readonly' , $daaBase['readonly']  ? 'yes':'no' );
		
		$daaBase['options']  = json_decode(@file_get_contents($structFile),true);
		
		return $daaBase;
	} 
	/**
	 * Create an address archive
	 * 
	 * @param string $address
	 * @param boolean $compress
	 * @param array $parameters
	 * @return boolean
	 */
	public function create($address, $compress = false, $parameters = array() )
	{
		if( $compress == true && (!function_exists('gzcompress') || !function_exists('gzuncompress') )) {
			$this->_exception("gzcompress() and gzuncompress() functions are required");
		} 
		
		$address =  (isset($address['address'])) ? $address['address'] : $address;
		
		$daaBase      = $this->lookup($address);
		$structFile   = $this->basename($address);
		$daaDirectory = $this->dirname($address);
		
		$this->_log(7, __FUNCTION__, __LINE__ , $address , 'creating address');
		
		if(!file_exists($daaDirectory) && !mkdir($daaDirectory)) {
			$this->_exception("Unable to create address directory");
		}
			
		$info                  = array();
		$info['ADDRESS']       = $address;
		$info['VERSION']       = $this->_version;
		$info['COMPRESS']      = $compress;
		$info['CREATION_DATE'] = time();
		$info['PARAMETERS']    = $parameters;
		$info['ROTATE']        = 0;
		$info['ROTATION']      = array();
		$info['ROWS']          = 0;
		$info                  = json_encode($info);
		
		if ( @file_put_contents($structFile, $info) != mb_strlen($info) ) {
			$this->_exception("Unable to create address info struct");
		}
		
		if(  !touch($daaBase['index']) || !touch($daaBase['digest'])  ) {
			$this->_exception("Unable to create digest or index files");
		}
		
		return true;
	}
	
	/**
	 * open an address archive
	 * 
	 * @param string $address
	 * @return array
	 */
	public function open($address)
	{
		$structFile   = $this->basename($address);
		$daaDirectory = $this->dirname($address);
		$daaBase      = $this->lookup($address);
	
		if( $daaBase['exists'] == false ) {
			$this->_exception("Address not found");
		}
		
		return ($this->_address  = $daaBase );
	}
	
	/**
	 * map index and digest
	 * 
	 * @param int $offset
	 * @param int $limit
	 * @param boolean $b64
	 * @return array
	 */
	public function map($offset, $limit, $b64 = true)
	{
		if($this->_address == null) {
			$this->_exception("address not open");
		}
				
		$offset = intval($offset);
		$limit = intval($limit);
		if($offset < 0) $offset = 0;
		if($limit > 25) $limit = 25;
		elseif($limit <= 0) $limit = 25;
		
		$indexFd = fopen($this->_address['index'],'r');
		$done = false;
		$piece = array();
		$lineCount = 0;
		for($lineCount = 0; !feof($indexFd); $lineCount++) {
			$line = fgets($indexFd);
			if($lineCount < $offset) continue; // skipping
			if($lineCount > $limit + $offset)break; //done slicing
			$piece[] = $line;
		}
		$out = array(
			'tot' => $this->_address['options']['ROWS'],
			'messages'=>array()
		);
		
		if($piece <= 0) {
			$this->_exception("offset+limit out of boundaries");
		}
		
		$digestFd = fopen($this->_address['digest'], 'r');
		
		foreach($piece as $idx => $line) {
			if(trim($line) == '')
				continue;
			
			$line  = explode(';', $line);
		
			$this->_log(9, __FUNCTION__, __LINE__ , 'line',serialize($line));
			//var_dump($line);	
			$data = $this->_read($digestFd, $line[1], $line[2]) ;
			
			$out['messages'][$line[4]][] = array(
				'id' => $line[3],
				'timestamp' => $line[0],
				'data' => $b64 ? base64_encode($data) : $data,
			);			
		}
		
		fclose($digestFd);
		
		return $out;
	}
	/**
	 * Read at $bPos for $bCount bytes from $fd
	 * 
	 * @param resource $fd
	 * @param int $bPos
	 * @param int $bCount
	 * @return NULL|string
	 */
	private function _read($fd, $bPos, $bCount)
	{
		if($this->_address == null) {
			$this->_exception("address not open");
		}
		
		$bPos = intval($bPos);
		$bCount = intval($bCount);
		
		if ( fseek($fd, $bPos , SEEK_SET) == -1 ) {
			$this->_log(1, __FUNCTION__, __LINE__ , 'seek failed');
			return null;
		}
		
		if ( ( $data = fread($fd, $bCount) ) === false ) {
			$this->_log(1, __FUNCTION__, __LINE__ , 'read failed');
			return null;
		}
		
		if($this->_address['options']['COMPRESS'] === true) {
			$this->_log(9, __FUNCTION__, __LINE__ , 'deflate');
			if ( (  $data = gzuncompress($data) ) === false ) {
				$this->_log(1, __FUNCTION__, __LINE__ , 'unzip failed');
				return null;
			}
		}
		
		return $data;
	}
	
	/**
	 * Store a message into the digest
	 * 
	 * @param string $payload
	 * @param array $parameters
	 * @return boolean
	 */
	public function write($payload, $parameters = array())
	{
		if($this->_address == null) {
			$this->_exception("address not open");
		}
		if(mb_strlen($payload) == 0 || mb_strlen($payload) > 1024 ) {
			$this->_exception("invalid payload");
		}	
		clearstatcache(true, $this->_address['digest']);
		clearstatcache(true, $this->_address['index']);
		
		$this->_log(5, __FUNCTION__, __LINE__ , 'called',serialize(func_get_args()));

		$this->_log(8, __FUNCTION__, __LINE__ , 'PRIOR_WRITE' , 'filesize' , $this->_address['digest'] ,  filesize($this->_address['digest']) );
		
		$index = array( time(), filesize($this->_address['digest']));
			
		if($this->_address['options']['COMPRESS']) {
			$this->_log(9, __FUNCTION__, __LINE__ , 'compressing');
			$payload = gzcompress($payload,9);
		}
		
		$this->_log(8, __FUNCTION__, __LINE__ , 'WRITING' ,  mb_strlen($payload) , 'bytes' );
		
		if ( ($bytes = file_put_contents($this->_address['digest'], $payload, FILE_APPEND)) != mb_strlen($payload) ) {
			$this->_exception("Unable to write the message to the digest file");
		}

		clearstatcache(true, $this->_address['digest']);
		
		$this->_log(8, __FUNCTION__, __LINE__ , 'WRITTEN' , $bytes, 'bytes' );
		
		$this->_log(8, __FUNCTION__, __LINE__ , 'POST_WRITE' , 'filesize' , $this->_address['digest'] ,  filesize($this->_address['digest']) );
		
		$index[] = mb_strlen($payload);
		
		$index[] = microtime(true);
		
		if(is_array($parameters) && !empty($parameters)) {
			foreach($parameters as $p) {
				$index[] = $p;
			}
		}

		$index = implode(';',$index) . PHP_EOL;
		
		if ( file_put_contents($this->_address['index'], $index, FILE_APPEND) != mb_strlen($index) ) {
			$this->_exception("Unable to write the cursor to the index file");
		}
		
		clearstatcache(true, $this->_address['index']);
		
		$this->_address['options']['ROWS']++;
		
		$this->save();
		
		return true;
	}
	
	/**
	 * Rotate the archive files
	 * 
	 * @return boolean
	 */
	public function rotate()
	{
		if($this->_address == null) {
			$this->_exception("address not open");
		}
		
		$rotator = $this->_address['options']['ROTATE'];

		if ( !rename($this->_address['index'],$this->_address['index']. '.' . $rotator) ) {
			$this->_exception("Unable to archive index file");
		} else {
			$this->_log(5, __FUNCTION__, __LINE__ , 'rotate ',$this->_address['index'] , $this->_address['index']. '.' . $rotator);
		}
		
		if ( ! rename($this->_address['digest'],$this->_address['digest']. '.' . $rotator)) {
			$this->_exception("Unable to archive digest file");
		} else {
			$this->_log(5, __FUNCTION__, __LINE__ , 'rotate ',$this->_address['digest'] , $this->_address['digest']. '.' . $rotator);
		}
		

		if (!touch($this->_address['index'])) {
			$this->_exception("Unable to create index file");
		}
		if (!touch($this->_address['digest'])) {
			$this->_exception("Unable to create digest file");
		}
		
		$rotator++;
		
		@$this->_address['options']['ROTATE'] = $rotator;
		@$this->_address['options']['ROTATION'][$rotator] = $this->_address['options']['ROWS'];
		@$this->_address['options']['ROWS']   = 0;
		$this->save();
		
		return true;
	}
	
	/**
	 * update the struct file
	 * 
	 * @return boolean
	 */
	public function save()
	{
		if($this->_address == null && $silent )
			return;
		else if($this->_address == null) {
			$this->_exception("address not open");
		}
		
		$info = json_encode($this->_address['options']);
		
		if(@file_put_contents($this->basename($this->_address['address']), $info) != mb_strlen($info)) {
			$this->_exception("unable to update strcut file");
		}
		
		return true;
	}
	
	public function __destruct()
	{
		if($this->_address != null)
			$this->save(true);
	}
	
	private function _log()
	{
		$args = func_get_args();
		$level = array_shift($args);
		if($level > $this->logLevel) return;
		$identifier = array_shift($args);
		$line = array_shift($args);
		$message = implode("\t", $args);
		$log = date('YmdHis') . "\t" . $level . "\t" . __CLASS__ . '::' . $identifier . "[{$line}]\t" . $message . PHP_EOL ;
		print $log;
	} 
	
	private function _exception($e)
	{
		$this->_log(1, __FUNCTION__, __LINE__ , $e);
		throw new \Exception($e);
	}
}
