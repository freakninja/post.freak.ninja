<?php
namespace freakNinja;
/**
 * PostBoard
 * 
 * @author "valerio tesei" <v@freak.ninja>
 *
 */
class PostBoard
{
	/**
	 * if true RSAKey generation/return can be made over unsecure HTTP request
	 * 
	 * @var boolean
	 */
	public $allowUnsecureKeySharing = false;
	
	/**
	 * logLevel
	 * @var int
	 */
	public $logLevel = 0;
	
	/**
	 * 
	 * @var DirectAccessArchive
	 */
	private $_daa = null;
	
	/**
	 * Server ADDRESS
	 * 
	 * @var array
	 */
	private $_serverAddress = null;
	
	/**
	 * if true message payload is gzipped
	 * 
	 * @var boolean
	 */
	public $compressArchive = false;
	
	public function __construct($dataFolder)
	{
		$this->_log(5, __FUNCTION__, __LINE__ , 'called',serialize(func_get_args()));
		
		if(!function_exists('mb_strlen') || !function_exists('mb_strlen') ) {
			$this->_exception("mb_strlen() function is required");
		} 
		
		$this->_daa = new DirectAccessArchive($dataFolder);
	}
	
	/**
	 * Handle Addresses creation and lookup.
	 * 
	 * @param mixed $address
	 * @param string $create
	 * @param string $publicKeyPayload
	 * @return array {address}, false on error
	 */
	private function _address($address, $create = false, $publicKeyPayload = null)
	{
		$this->_log(5, __FUNCTION__, __LINE__ , 'called',serialize(func_get_args()));
		
		$address = $this->_daa->lookup($address);
		
		if( $address['exists'] == false && $create == false ) {
			return false;
		} else if (($publicKeyPayload == null || mb_strlen($publicKeyPayload) == 0) && $create == true ) {
			$this->_exception("Public Key not valid");
		}
		
		if($create == true ) {
			$properties = array(
				'publicKey' => $publicKeyPayload,
				'flushKey'  => uniqid(null,true)
			);
			$this->_daa->create($address, $this->compressArchive, $properties);
		}
				
		return $this->_daa->open($address);
	}
	
	/**
	 * Set the compression flag on/off
	 * 
	 * @param boolean $set
	 * @return boolean
	 */
	public function compress($set = true)
	{
		$this->_log(5, __FUNCTION__, __LINE__ , 'called',serialize(func_get_args()));
		
		$this->compressArchive = $set === true;
		
		return ($this->compressArchive);
	}
	
	/**
	 * Sets server's public key
	 * 
	 * @param string $serverAddress main server address
	 * @param string $serverPublicKey PEM public certificate
	 */
	public function setServerPublicKey($serverAddress, $serverPublicKey)
	{
		$this->_log(5, __FUNCTION__, __LINE__ , 'called',serialize(func_get_args()));
		
		if(!is_readable($serverPublicKey) ) {
			$this->_exception("RSA Public key is not readable");
		}
		
		if ($serverAddress == '') {
			$this->_exception("ServerAddress name is mandatory");
		}
		
		$this->_serverAddress = $this->_address($serverAddress);
		
		if(!$this->_serverAddress) {
			$this->_log(8, __FUNCTION__, __LINE__ , 'creating server address');
			$this->_serverAddress = $this->_address($serverAddress,true, file_get_contents($serverPublicKey) );
		}
		
		$this->_log(9, __FUNCTION__, __LINE__ , 'serverConfig',serialize($this->_serverAddress));
	}
	
	/**
	 * Post on an address.
	 * 
	 * @param mixed $fromAddress sender's address 
	 * @param mixed $toAddress rcpt's address
	 * @param string $message  message string 
	 * @param string $signature binary signature of sender's address signed with sender's privKey
	 * @return boolean
	 */
	public function post($fromAddress , $toAddress, $message, $signature)
	{
		$this->_log(5, __FUNCTION__, __LINE__ , 'called',serialize(func_get_args()));
		$fromAddress = $this->_address($fromAddress);
		$toAddress   = $this->_address($toAddress);
		
		if ( mb_strlen($message) <= 0 || mb_strlen($message) > 1024)
			$this->_exception('message parameter > 0 len, <= 1024');
		
		if($fromAddress == false ) {
			$this->_exception("source Address not found");
		}
		
		if($toAddress == false ) {
			$this->_exception("destination Address not found");
		}
		
		$this->_log(9, __FUNCTION__, __LINE__ , $fromAddress['address'], $toAddress['address'], $message, $signature );
		
		$fromAddressPUB = $fromAddress['options']['PARAMETERS']['publicKey'];
		
		if ( !$this->_verify($fromAddress['address'], $signature,$fromAddressPUB) ) {
			$this->_exception("unable to verify from address");
		}
		
		if ( $this->_daa->write($message, array($fromAddress['address'],bin2hex($signature))) ) {
			return  true;
		}
	}
	
	/**
	 * Create an address and store its public key
	 * 
	 * @param string $address
	 * @param string $publicKeyPayload PEM public key
	 * @return mixed
	 */
	public function create($address,$publicKeyPayload)
	{
		$this->_log(5, __FUNCTION__, __LINE__ , 'called',serialize(func_get_args()));
		
		$rsa = new \Crypt_RSA();
		$rsa->loadKey($publicKeyPayload);
		
		if($rsa->getSize()  < 4096) {
			$this->_exception("Public Key MUST be at least 4096bits");
		}
		
		if($this->_address($address) !== false) {
			$this->_exception("Address already in use");
		}
		
		return $this->_address($address,true, $publicKeyPayload);
	}
	
	/**
	 * List messages
	 * 
	 * @param string $address
	 * @param int $offset
	 * @param int $limit
	 * @param boolean $escape if true messages payload is base64 encoded, binary otherwise
	 */
	public function messages($address, $offset = 0, $limit = 25, $escape = true)
	{
		$this->_log(5, __FUNCTION__, __LINE__ , 'called',serialize(func_get_args()));
		$address = $this->_address($address);
		
		if($address == false ) {
			$this->_exception("Address not found");
		}
		
		return $this->_daa->map($offset,$limit,true);
	}
	
	/**
	 * Generate a key pair
	 * 
	 * @return array(private, public, note)
	 */
	public function generate()
	{
		$this->_log(5, __FUNCTION__, __LINE__ , 'called',serialize(func_get_args()));
		
		if( (!$this->_isHTTPS() && !$this->allowUnsecureKeySharing) && !$this->_isCLI()) {
			$this->_exception("Keys can be exchanged only over SSL");
		}
		$out = array();
		$rsa = new \Crypt_RSA();
		$rsa->setPrivateKeyFormat(CRYPT_RSA_PRIVATE_FORMAT_PKCS1);
		$rsa->setPublicKeyFormat(CRYPT_RSA_PUBLIC_FORMAT_PKCS8);
		$keys = $rsa->createKey(4096);
		$out['private'] = $keys['privatekey'];
		$out['public']  = $keys['publickey'];
		$out['note'] = 'once returned the privateKey and publicKey are lost forever';
		return $out;
	}
	
	/**
	 * Lookup an address
	 * 
	 * @param string $address
	 * @return mixed array on success, false otherwise
	 */
	public function lookup($address)
	{
		$address = $this->_address($address);
		
		if(!$address)
			return false;
		
		return array($address['address'], $address['options']['PARAMETERS']['publicKey']);
	}
	
	/**
	 * Rotate messages (archives current messages)
	 * 
	 * @param string $address
	 * @param string $sign
	 * @return boolean
	 */
	public function flush($address, $sign = '')
	{
		$address = $this->_address($address);
		
		if(!$address)
			$this->_exception("Address not found");
		
		$flushKey = $address['options']['PARAMETERS']['flushKey'];
		
		$publicKey = $address['options']['PARAMETERS']['publicKey'];
		
		if( $sign !='' && $this->_verify($flushKey, $sign , $publicKey)  ) {
			if ( $this->_daa->rotate() ) {
				return true;
			}
		} else if ( $sign !='' ) {
			$this->_exception("Invalid signature");
		} else {
			return $flushKey;
		}
		
		return false;
	}
	/**
	 * verify a signature
	 * 
	 * @param string $message
	 * @param string $signature
	 * @param string $publicKey
	 * @return int 0 wrong signature, 1 correct, -1 error
	 */
	private function _verify($message, $signature ,$publicKey)
	{
		$this->_log(5, __FUNCTION__, __LINE__ , 'called', serialize(func_get_args()));
		return openssl_verify($message, $signature, $publicKey);
	}

	/**
	 * true if over SSL
	 * 
	 * @return boolean
	 */
	private function _isHTTPS() 
	{
		$this->_log(5, __FUNCTION__, __LINE__ , 'called',serialize(func_get_args()));
		return (!empty($_SERVER['HTTPS']) && @$_SERVER['HTTPS'] !== 'off') || @$_SERVER['SERVER_PORT'] == 443;
	} 
	/**
	 * true if php-cli 
	 * 
	 * @return boolean
	 */
	private function _isCLI()
	{
		$this->_log(5, __FUNCTION__, __LINE__ , 'called',serialize(func_get_args()));
		
		return (php_sapi_name() === 'cli');
	}
	/**
	 * Log & Exception
	 * 
	 * @param string $e
	 * @throws \Exception
	 */
	private function _exception($e)
	{
		$this->_log(1, __FUNCTION__, __LINE__ , $e);
		throw new \Exception($e);
	}
	/**
	 * Log, whatever. 
	 */
	private function _log()
	{
		$args = func_get_args();
		$level = array_shift($args);
		if($level > $this->logLevel) return;
		$identifier = array_shift($args);
		$line = array_shift($args);
		$message = implode("\t", $args);
		$log = date('YmdHis') . "\t" . $level . "\t" . __CLASS__ . '::' . $identifier . "[{$line}]\t" . $message . PHP_EOL ;
		print $log;
	} 
}
