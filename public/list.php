<?php
require 'init.php';

$address = @$_REQUEST['address'];

if($address == '')
	throw new \Exception('missing address parameter');

$offset = 0;
if(isset($_REQUEST['offset']))
	$offset = $_REQUEST['offset'];

$limit = 25;
if(isset($_REQUEST['limit']))
	$limit = $_REQUEST['limit'];
		
jsonize($PostBoard->messages($address, $offset, $limit));