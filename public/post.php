<?php
require 'init.php';

$from      = @$_REQUEST['from'];
$to        = @$_REQUEST['to'];
$message   = @base64_decode(@$_REQUEST['m']);
$signature = @base64_decode(@$_REQUEST['s']);
if($from == '')
	throw new \Exception('missing from parameter');
if($to == '')
	throw new \Exception('missing to parameter');
if($message == '')
	throw new \Exception('missing message parameter');
if(strlen($message) <= 0 || strlen($message) > 700 )
	throw new \Exception('message parameter > 0 len, <= 700 ');
if($signature == '')
	throw new \Exception('missing signature parameter');


$PostBoard->post($from, $to, $message, $signature);

jsonize('OK');