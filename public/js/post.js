var postJs = (function(){
	var URL = '//post.freak.ninja';
	var cache = function(k,v)
	{
		k = window.location.host + '_' + k;
		if(typeof v != 'undefined') 
			localStorage[k] = v;
		
		return localStorage[k];
	}
	var cachebuster = function() {  
		return  '?' + Math.round(new Date().getTime() / 1000); 
	};
	var wipe = function()
	{
		for (var k in localStorage) {
			delete localStorage[k];
		}
	}
	var Debug  = true;
	var Keys =
	{
			PublicKey : function(value) {
				return cache('PublicKey',value);
			},
			PrivateKey : function(value) {
				return cache('PrivateKey',value);
			},
			Sign : function(text)
			{
				var rsa = new RSAKey();
				rsa.readPrivateKeyFromPEMString(Keys.PrivateKey());
				var hashAlg = 'sha1';
				var hSig = rsa.sign(text, hashAlg);
				return hex2b64(hSig);
			},
			Decrypt : function(encrypted)
			{
				var crypt = new JSEncrypt();
				crypt.setPrivateKey(Keys.PrivateKey());
				return crypt.decrypt(encrypted);
				
			},
			Encrypt : function(plain, publicKey)
			{
				var crypt = new JSEncrypt();
				crypt.setPublicKey(publicKey);
				return crypt.encrypt(plain);
			},
	}
	
	
	var postJs = function()
	{
		if(!localStorage || !localStorage.getItem || !localStorage.setItem )
			throw new "localStorage is required";
		if(!JSEncrypt)
			throw new "JSEncrypt is required";
		
		Engine = new JSEncrypt();
		Signer = new JSEncrypt();
		
		if( cache('fromAddress') ) {
			this._fromAddress = cache('fromAddress');
		}
		
	};
	
	postJs.prototype._jsError = function(status, statusText, body, isJson)
	{
		if (isJson && body.error) {
			alert(body.error);
		}
		if(console && console.log && Debug == true)
			console.log(status, statusText, body, isJson);
	};
	
	postJs.prototype.setPassword = function(password)
	{
		this._password = password;
	};
	
	postJs.prototype.hasIdentify = function(onSuccess, onError)
	{
		var self = this;
		
		var errorCallback = function(status, statusText, body, isJson) {
			if( onError && onError.call )
				onError.call(window, status, statusText, body, isJson);
			else
				self._jsError(status, statusText, body, isJson);
			return false;
		}
		var identity = cache('fromAddress') != '' ?  cache('fromAddress') : false;
		
		if(!identity || identity == '')
			return errorCallback(200,'Internal Server Error', {error:'Address not found'},true);
		
		requestJS.post(URL + '/' + identity +  '/whois', {}, true, function(json){
			onSuccess(json);
		},errorCallback);
	};
	
	postJs.prototype.setAddress = function(address)
	{
		return (this._fromAddress = cache('fromAddress',address));
	};
	
	postJs.prototype.setPublicKey = function(Pk)
	{
		return Keys.PublicKey(Pk);
	};

	postJs.prototype.setPrivateKey = function(pk)
	{
		return Keys.PrivateKey(pk);
	};

	postJs.prototype.register = function(identity, privateKey, publicKey, onSuccess, onError)
	{
		var self = this;
		
		var errorCallback = function(status, statusText, body, isJson) {
			if( onError && onError.call )
				onError.call(window, status, statusText, body, isJson);
			else
				self._jsError(status, statusText, body, isJson);
			return false;
		}
		if(!identity || identity == '')
			return errorCallback(500,'Internal Server Error', {error:'missing address parameter'},true);
		
		if(!privateKey || !publicKey || privateKey =='' || publicKey =='') {
			return errorCallback(500,'Internal Server Error', {error:'Missing private key or public key'},true);
		}
		
		requestJS.post(URL + '/' + identity +  '/whois', {}, true, function(json){
			alert('Address already in use');
		}, function(status, statusText, body, isJson) {
			if(isJson && body.error == 'Address not found') {
				requestJS.post(URL + '/' + identity +  '/register', {pKEY : publicKey }, true, function(json){
					Keys.PrivateKey(privateKey);
					Keys.PublicKey(publicKey);
					if ( json.error ) {
						throw new "Unable to register your identity : " + json.error;
					}
					self._fromAddress = cache('fromAddress',identity);
					if( onSuccess && onSuccess.call )
						onSuccess.call(window, json);
				}, errorCallback);
			} else {
				errorCallback(status, statusText, body, isJson);
			}
		});
	};
	
	postJs.prototype.generate = function(onSuccess, onError)
	{
		var self = this;
		
		var errorCallback = function(status, statusText, body, isJson) {
			if( onError && onError.call )
				onError.call(window, status, statusText, body, isJson);
			else
				self._jsError(status, statusText, body, isJson);
			return false;
		}
		requestJS.post(URL + '/generate', {}, true, function(json){
			if(onSuccess && onSuccess.call)
				onSuccess.call(window,json);
		}, errorCallback);
	}
	
	postJs.prototype.whois = function(address, onSuccess, onError)
	{
		var self = this;

		var errorCallback = function(status, statusText, body, isJson) {
			if( onError && onError.call )
				onError.call(window, status, statusText, body, isJson);
			else
				self._jsError(status, statusText, body, isJson);
			
			return false;
		}
		
		if(!address || address == '')
			return errorCallback(500,'Internal Server Error', {error:'missing address parameter'},true);
		
		requestJS.post(URL + '/' + address +  '/whois'+ cachebuster(), {}, true, function(json){
			if(onSuccess && onSuccess.call)
				onSuccess.call(window,json);
		},errorCallback);
	}
	
	postJs.prototype.post = function(to, message, onSuccess, onError)
	{
		var self = this;
		
		var errorCallback = function(status, statusText, body, isJson) {
			if( onError && onError.call )
				onError.call(window, status, statusText, body, isJson);
			else
				self._jsError(status, statusText, body, isJson);
			
			return false;
		}
		
		if (!to || to.length > 50 || to.length <= 0) {
			return errorCallback(500,'Internal Server Error', {error:'toAddress too long or too short'},true);
		}
		
		if (!message || message.length > 500 || message.length <= 0) {
			return errorCallback(500,'Internal Server Error', {error:'Message too long or too short'},true);
		}

		requestJS.post(URL + '/' + to +  '/whois'+ cachebuster(), {}, true, function(json){
			var publicKey = json[1];
			var to = json[0];
			message = Keys.Encrypt(message, publicKey);
			requestJS.post(URL + '/' + to +  '/post'+ cachebuster(), {
				from : self._fromAddress,
				to   : to,
				m    : message,
				s    : Keys.Sign(self._fromAddress)
			}, true,  function(json){
				if( onSuccess && onSuccess.call )
					onSuccess.call(window, json);
				else
					if(console && console.log) 
						console.log('message delivered');
			}, errorCallback);
		},errorCallback);
		 
		
	};
	
	postJs.prototype.messages = function(offset, limit,onSuccess, onError)
	{
		var self = this;
		
		var errorCallback = function(status, statusText, body, isJson) {
			if( onError && onError.call )
				onError.call(window, status, statusText, body, isJson);
			else
				self._jsError(status, statusText, body, isJson);
		}
		
		if(!offset) offset = 0;
		if(!limit) limit = 25;
		
		var params = {
			offset : offset,
			limit  : limit,
		}
		
		requestJS.post(URL + '/' + this._fromAddress +  '/list'+ cachebuster(), params, true, function(json){
			if(json.tot <= 0)
				return;
			if(onSuccess && onSuccess.call ){
				onSuccess.call(window,json);
			} else if (console && console.log) {
				console.log(json.messages);
			}
		},errorCallback);
	};
	
	postJs.prototype.decrypt = function(enc)
	{
		return Keys.Decrypt(enc);
	}
	
	postJs.prototype.doTest = function()
	{
		if(Keys.PublicKey() == '' || Keys.PrivateKey() == '')
			return false;
		
		var plain = 'this is a string message';
		var encrypted = Keys.Encrypt(plain, Keys.PublicKey());
		var decrypted = Keys.Decrypt(encrypted);
		if(decrypted != plain) {
			return false;
		}
		return true;
	};
	
	return postJs;
})();