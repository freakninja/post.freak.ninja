"use strict";
var requestJS = (function(){
	
	var request = function()
	{
		this.queue = [];
		this.isRunning = false;
		this.counter = 0;
	};
	/**
	 * create a XMLHttpRequest object
	 * 
	 * @returns [object XMLHttpRequest]
	 */
	request.prototype._newXHMLHttp = function()
	{
		if(console && console.log && this.debug)
			console.log('requestJS._newXHMLHttp');
		var http_request; try { http_request = new XMLHttpRequest(); } catch (e) { try { http_request = new ActiveXObject("Msxml2.XMLHTTP"); } catch (e) { try { http_request = new ActiveXObject("Microsoft.XMLHTTP"); } catch (e) { return false; } } } 
		return http_request;	
	}
	/**
	 * Execute the next call in the stack
	 * 
	 * @returns {void}
	 */
	request.prototype._dequeue = function()
	{
		if(this.queue.length == 0)
			return ( (console && console.log && this.debug) ? console.log('requestJS._dequeue', 'queue len is zero') : false);
		if(this.isRunning) 
			return ( (console && console.log && this.debug) ? console.log('requestJS._dequeue', 'another request is running') : false);
		
		this.isRunning = true;
		var self = this;
		var c = this._newXHMLHttp();
		c.element = self.queue.pop();
		c.element.requestId = requestJS.counter++;
		if(console && console.log && this.debug)
			console.log('requestJS.'+c.element.requestId+'._dequeue', c.element.url, c.element.params);
		c.open('POST', c.element.url , true);
		c.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		c.onreadystatechange = function(){
			if ( c.readyState != 4 ) 
				return;
			
			if(console && console.log && this.debug)
				console.log('requestJS.'+c.element.requestId+'._done', c.element.url, c.element.params);
			
			var body = c.responseText, isError = false;
			
			self.isRunning = false;
			self._dequeue();
			var isError = false;
			
			if( (c.status != 200 && c.status != 0) || c.responseText == null )
				isError = true;
			
			
			if(typeof c.element.onSuccess == 'function' && !isError ) {
				if(c.element.isJson == true) {
					try { 
						body = JSON.parse(body);
					} catch(e) {
						body = c.responseText;
						isError = true;
					}
				}
				if(!isError)
					c.element.onSuccess(body);
			} 
			
			if(isError && typeof c.element.onError == 'function') {
				var isJson = false;
				try { 
					body = JSON.parse(body);
					if(body == null && isJson) {
						body = c.responseText;
					} else {
						isJson = true;
					}
				} catch(e) {
					body = c.responseText;
				} 
				c.element.onError(c.status, c.statusText, body,isJson);
			}
			
		};
		
		c.send(c.element.params);
		
	};
	/**
	 * Insert a post request into the stack
	 * 
	 * @params url
	 * @params params
	 * @params isJson
	 * @params fnSuccessCallback
	 * @params fnErrorCallback
	 */
	request.prototype.post = function(url, params, isJson, fnSuccessCallback, fnErrorCallback)
	{
		if(console && console.log && this.debug)
			console.log('requestJS.post',url, params, isJson, fnSuccessCallback, fnErrorCallback);
		var _params = [];
		for(var k in  params) {
			_params.push(encodeURIComponent(k) + '=' + encodeURIComponent(params[k]));
		}

		this.queue.push({
			url :  url , 
			params : _params.join('&'),
			isJson : isJson,
			onSuccess : fnSuccessCallback,
			onError : fnErrorCallback
		});
		
		this._dequeue();
		
		return this;
	};
	return new request();
})();

