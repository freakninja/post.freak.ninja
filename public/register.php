<?php
require 'init.php';

$address = @$_REQUEST['address'];
$pubkey  = @$_REQUEST['pKEY'];

if($address == '')
	throw new \Exception('missing address parameter');
if($pubkey == '')
	throw new \Exception('missing pKEY parameter');

$PostBoard->create($address, $pubkey);

jsonize('OK');