<?php
set_include_path(get_include_path() . PATH_SEPARATOR . '../library/'. PATH_SEPARATOR . '../library/phpseclib');
require_once('Crypt/RSA.php');
require_once('freakNinja/PostBoard.php');
require_once('freakNinja/DirectAccessArchive.php');

function jsonize($what)
{
	$out = json_encode($what); 
	header('Content-Type: application/json');
	header('Content-Length: ' . strlen($out));
	print $out;
	exit;
}

set_exception_handler(function( Exception $ex ){
	http_response_code(500);
	jsonize(array('error' => $ex->getMessage()));
});

$PostBoard = new \freakNinja\PostBoard( '../data/');
$PostBoard->allowUnsecureKeySharing = true;