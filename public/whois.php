<?php
require_once('init.php');

$address = @$_REQUEST['address'];
		
if($address == '')
	throw new \Exception('missing address parameter');

$address = $PostBoard->lookup($address);

if(!$address)
	throw new \Exception('Address not found');

jsonize($address);
