<!DOCTYPE html>
<html>
	<head>
		<title>freakMessenger</title>
		<script type="text/javascript" src="js/jsrsasign-4.8.3-all-min.js"></script>
		<script type="text/javascript" src="js/jsencrypt.min.js"></script> 
		<script type="text/javascript" src="js/request.js"></script>
		<script type="text/javascript" src="js/post.js"></script>
		<script type="text/javascript">
			var pjs = new postJs;

			var messages = [];
			
			function doSaveProfile()
			{
				var address = document.getElementById('txt_register_name').value;
				var publicKey = document.getElementById('txt_rsa_public').value;
				var privateKey = document.getElementById('txt_rsa_private').value;
				
				pjs.whois( address, function(json) {
					pjs.setAddress(address);
					pjs.setPublicKey(publicKey);
					pjs.setPrivateKey(privateKey);
					alert('doSaveProfile : SUCCESS');
					window.location = window.location;
				}, function(status, statusText, body, isJson) {
					alert('doSaveProfile : ERROR[' + statusText + '] = ' + body.error );
				});
			}

			function doGenerateKey()
			{
				alert("This may take some time, hit OK and please wait...");
				pjs.generate( function(json) {
					console.log(json);
					document.getElementById('txt_rsa_public').value  = json['public'];
					document.getElementById('txt_rsa_private').value  = json['private'];
					alert('doGenerateKey : SUCCESS');
				}, function(status, statusText, body, isJson) {
					alert('doGenerateKey : ERROR[' + statusText + '] = ' + body.error );
				});
			}
			
			function doRegister() {
				var publicKey = document.getElementById('txt_rsa_public').value;
				var privateKey = document.getElementById('txt_rsa_private').value;
				
				pjs.register(document.getElementById('txt_register_name').value,privateKey,publicKey,function(json){
					alert('doRegister : SUCCESS');
					window.location = window.location;
				}, function(status, statusText, body, isJson) {
					alert('doRegister : ERROR[' + statusText + '] = ' + body.error );
				});
			}

			function doPost() {
				pjs.post(document.getElementById('txt_message_to').value,document.getElementById('txt_message_body').value,
					function(json){
						alert('doPost : SUCCESS');
						window.location = window.location;
					}, function(status, statusText, body, isJson) {
						alert('doPost : ERROR[' + statusText + '] = ' + body.error );
					}
				);
			}	

			function doUpdate() {		
				pjs.messages(0,25,function(json) {
					var html = '', m = '' ;
					for(var address in json.messages) {
						for (var msgId in json.messages[address]) {
							m = json.messages[address][msgId];
							if(!document.getElementById('msg-'+m.id)) {
								var date = new Date(m.timestamp * 1000);
								m.data = pjs.decrypt(m.data);
								html += '<span id="msg-'+m.id+'"><small>['+date.toLocaleString()+']</small>&nbsp;&nbsp;<strong>'+address+'</strong>:&nbsp;'+m.data + '<br />';
							}
						}
					}
					if(html != '')
						document.getElementById('messages').innerHTML = document.getElementById('messages').innerHTML+html;
				}, function(status, statusText, body, isJson) {
					//alert('doUpdate : ERROR[' + statusText + '] = ' + body );
				});
				setTimeout(doUpdate,3000);
			}
		</script>
	</head>
	<body>
		<h1>freakMessenger</h1>
		<small>Secure message board using SSL</small>
		<h2>Proof of concept:</h2>
		<ul>
			<li>RSA Based message board</li>
			<li>500 Characters per message</li>
			<li>RSA 4096bit keys required</li>
			<li>user register : https://post.freak.ninja/{user}/register POST[pKEY:publicKey]</li>
			<li>user lookup : https://post.freak.ninja/{user}/whois</li>
			<li>message listing : https://post.freak.ninja/{user}/list</li>
			<li>post message : https://post.freak.ninja/{user}/post POST[from:username, m:message, s:signature]</li>
		</ul>
		<hr />
		<div id="div_register_form">
			<p>Greetings Dr. Falken :)</p>
			<strong>RSA Private KEY</strong><br />
			<textarea id="txt_rsa_private" rows=4 cols=80></textarea><br />
			<strong>RSA Public KEY</strong><br />
			<textarea id="txt_rsa_public" rows=4 cols=80></textarea><br />
			<input type="button" onclick="doGenerateKey();" value="Do them for me!" />
			Name: <input type="text" value="" placeholder="your name" id="txt_register_name" />
			<input type="button" onclick="doRegister();" value="Register your name" />
			<input type="button" onclick="doSaveProfile();" value="Retreive Profile" />
		</div>
		<div id="div_messages" style="display:none;">
			Welcome <span id="span_name"></span>,<br />
			What follows is your messages list:<br />
			<div id="messages">
			
			</div>
			TO:<input type="text" value="" placeholder="send message to" id="txt_message_to" />
			WHAT: <input type="text" value="" placeholder="message" id="txt_message_body" />
			<input type="button" onclick="doPost();" value="Send" />
		</div>
		<script type="text/javascript">
			pjs.hasIdentify(function(json){
				document.getElementById('div_register_form').style.display= 'none';
				document.getElementById('div_messages').style.display= 'block';
				document.getElementById('span_name').innerHTML = json[0];
				doUpdate();
			},function(){
				document.getElementById('div_register_form').style.display= 'block';
				document.getElementById('div_messages').style.display= 'none';
			});
		</script>
	</body>
</html>
