<?php
set_include_path(get_include_path() . PATH_SEPARATOR . 'library/'. PATH_SEPARATOR . 'library/phpseclib');
require_once('Crypt/RSA.php');
require_once('freakNinja/DirectAccessArchive.php');
require_once('freakNinja/sslPostBoard.php');

$PostBoard = new \freakNinja\sslPostBoard( 'data/');

if(!file_exists('config/server')) {
	$pair = $PostBoard->generate();
	@file_put_contents('config/server.public', $pair['public']);	
	@file_put_contents('config/server.pr8', $pair['private']);
}

$PostBoard->setServerPublicKey('server','config/server.public');

if(!file_exists('config/address1.pub') ) {
	$address1 = $PostBoard->generate();
	@file_put_contents('config/address1.pub', $address1['public']);
	@file_put_contents('config/address1.pr8', $address1['private']);
}

if(!file_exists('config/address2.pub') ) {
	$address2 = $PostBoard->generate();
	@file_put_contents('config/address2.pub', $address2['public']);
	@file_put_contents('config/address2.pr8', $address2['private']);
}

$address1PR8 = @file_get_contents('config/address1.pr8');
$address1PUB = @file_get_contents('config/address1.pub');
if($PostBoard->lookup('address1') == false ) {
	$PostBoard->create('address1', $address1PUB);
}

$address2PR8 = @file_get_contents('config/address2.pr8');
$address2PR8 =  @file_get_contents('config/address2.pub');
if($PostBoard->lookup('address2') == false ) {
	$PostBoard->create('address2', $address2PR8);
}
print 'ADDRESS2 -> ADDRESS1' .PHPE_EOL;
$address2_sign = null;
openssl_sign('address2', $address2_sign, $address2PR8, OPENSSL_ALGO_SHA1);

$encMessage  = null;
openssl_public_encrypt('Such a lovely day for encryption! ' , $encMessage, $valerioPUB);
$PostBoard->post('no.one', 'valerio.tesei',$encMessage , bin2hex($signature) );

$encMessage  = null;
openssl_public_encrypt('Another lovely day for encryption! ' , $encMessage, $valerioPUB);
$PostBoard->post( 'address2' , 'address1' ,$encMessage , bin2hex($signature) );

$messages = $PostBoard->messages('address1');

foreach($messages['messages'] as $from => $ms) {
	foreach($ms as $m) {
		$decrypted = null;
		if ( openssl_private_decrypt(base64_decode($m['data']), $decrypted, $valerioPR8 ) ) {
			print $m['timestamp'] . "\t" . $from . "\t" . $decrypted . PHP_EOL;
		} else {
			print $m['timestamp'] . "\t" . $from . "\t" . 'UNABLE TO DECRYPT' . PHP_EOL;
		}
	}
}

$address1_sign = null;
openssl_sign('address1', $address1_sign, $address1PR8, OPENSSL_ALGO_SHA1);


/*
$flush = $PostBoard->flush('valerio.tesei');
$signature = null;
openssl_sign($flush, $signature, $valerioPR8, OPENSSL_ALGO_SHA1);
$flush = $PostBoard->flush('valerio.tesei',bin2hex($signature));

if($flush)
	return 'OK';
else 
	return 'ERR';*/